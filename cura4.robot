*** Settings ***

Library    SeleniumLibrary
Resource    Mes_Ressources.resource

*** Test Cases ***
BDD_case1_HealthcareProgram
    [Tags]    test_tagrobot
    Given Utilisateur se connecte à Healthcare
    Given Utilisateur connecté sue la page accueil
    When Utilisateur s'autentifie avec    John Doe    ThisIsNotAPassword 
    When Utilisateur renseigne les information obligatoire    ${JDD1.facility}
    When Utilisateur choisi HealthcareProgram    ${JDD1.HealthcareProgram}
    Then Verifier les informations    ${JDD1.facility}    ${JDD1.HealthcareProgram}
    And End of Test and Close Browser

BDD_case2_HealthcareProgram

    Given Utilisateur se connecte à Healthcare
    Given Utilisateur connecté sue la page accueil
    When Utilisateur s'autentifie avec    John Doe    ThisIsNotAPassword 
    When Utilisateur renseigne les information obligatoire    ${JDD2.facility}
    When Utilisateur choisi HealthcareProgram    ${JDD2.HealthcareProgram}
    Then Verifier les informations    ${JDD2.facility}    ${JDD2.HealthcareProgram}
    And End of Test and Close Browser

BDD_case3_HealthcareProgram

    Given Utilisateur se connecte à Healthcare
    Given Utilisateur connecté sue la page accueil
    When Utilisateur s'autentifie avec    John Doe    ThisIsNotAPassword 
    When Utilisateur renseigne les information obligatoire    ${JDD3.facility}
    When Utilisateur choisi HealthcareProgram    ${JDD3.HealthcareProgram}
    Then Verifier les informations    ${JDD3.facility}    ${JDD3.HealthcareProgram}    
    And End of Test and Close Browser